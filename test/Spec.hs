import Test.Hspec ( hspec, describe, it, shouldBe )
import Lib

main :: IO ()
main = hspec $ do
    describe "20. Valid Parentheses" $ do
        it "() are balanced brackets" $ do
            validParentheses "()" `shouldBe` True
        it "()[]{} are balanced brackets" $ do
            validParentheses "()[]{}" `shouldBe` True
        it "(] are unbalanced brackets" $ do
            validParentheses "(]" `shouldBe` False
            
    describe "21. Merge Two Sorted Lists" $ do
        it "[1,2,4] ++ [1,3,4] == [1,1,2,3,4,4]" $ do
            merge2SortedLists [1,2,4] [1,3,4] `shouldBe` [1,1,2,3,4,4]
        it "[] ++ [] == []" $ do
            merge2SortedLists [] [] `shouldBe` []
        it "[] ++ [0] == []" $ do
            merge2SortedLists [] [0] `shouldBe` [0]

    describe "26. Remove Duplicates from Sorted Array" $ do
        it "[0,0,1,1,1,2,2,3,3,4] has 5 unique elements [0,1,2,3,4]" $ do
            removeDuplicates [0,0,1,1,1,2,2,3,3,4] `shouldBe` [0,1,2,3,4]
        it "[1,1,2] has 2 unique elements [1,2]" $ do
            removeDuplicates [1,1,2] `shouldBe` [1,2]

    describe "27. Remove Element" $ do
        it "remove 3 from [3,2,2,3] = [2,2]" $ do
            removeElement 3 [3,2,2,3] `shouldBe` [2,2]
        it "remove 2 from [0,1,2,2,3,0,4,2] = [0,1,4,0,3]" $ do
            removeElement 2 [0,1,2,2,3,0,4,2] `shouldBe` [0,1,3,0,4]

    describe "28. Implement strStr()" $ do
        it "ll is index at 2 of hello" $ do
            strStr "hello" "ll" `shouldBe` Just 2
        it "bba is not in aaaaaa" $ do
            strStr "bba" "aaaaaa" `shouldBe` Nothing 
        it "[] is index at 0 of []" $ do
            strStr "" "" `shouldBe` Just 0
    
    describe "35. Search Insert Position" $ do
        it "Insert 5 at position 2 of [1,3,5,6] = [1,3,'5',5,6]" $ do
            searchInsertPosition [1,3,5,6] 5 `shouldBe` 2
        it "Insert 2 at position 1 of [1,3,5,6] = [1,'2',3,5,6]" $ do
            searchInsertPosition [1,3,5,6] 2 `shouldBe` 1
        it "Insert 7 at position 4 of [1,3,5,6] = [1,3,5,6,'7']" $ do
            searchInsertPosition [1,3,5,6] 7 `shouldBe` 4

    describe "53. Maximum Subarray" $ do
        it "The maximum sum of the consecutive subarry of [-2,1,-3,4,-1,2,1,-5,4] equals 6" $ do
            maxSubArray [-2,1,-3,4,-1,2,1,-5,4] `shouldBe` 6
        it "The maximum sum of the consecutive subarry of [1] equals 1" $ do
            maxSubArray [1] `shouldBe` 1
        it "The maximum sum of the consecutive subarry of [-2,1,-3,4,-1,2,1,-5,4] equals 6" $ do
            maxSubArray [5,4,-1,7,8] `shouldBe` 23

    describe "58. Length of Last Word" $ do
        it "The length of the last word of 'Hello World' is 5" $ do
            lengthOfLastWord "Hello World" `shouldBe` 5
        it "The length of the last word of '   fly me   to   the moon  ' is 4" $ do
            lengthOfLastWord "   fly me   to   the moon  " `shouldBe` 4
        it "The length of the last word of 'luffy is still joyboy' is 6" $ do
            lengthOfLastWord "luffy is still joyboy" `shouldBe` 6

    describe "66. Plus One" $ do
        it "[1,2,3] + 1 = [1,2,4]" $ do
            plusOne [1,2,3] `shouldBe` [1,2,4]
        it "[4,3,2,1] + 1 = [4,3,2,2]" $ do
            plusOne [4,3,2,1] `shouldBe` [4,3,2,2]
        it "[9] + 1 = [1,0]" $ do
            plusOne [9] `shouldBe` [1,0]

    describe "94. Binary Tree Inorder Traversal" $ do
        it "Inorder (Left, Root, Right) : 4 2 5 1 3 " $ do
            let t1 = Node (Node (Node Leaf 4 Leaf) 2 (Node Leaf 5 Leaf)) 1 (Node Leaf 3 Leaf)
            inorderTraversal t1 `shouldBe` [4,2,5,1,3]

    describe "94. Binary Tree Inorder Traversal" $ do
        it "Depth of tree || Inorder (Left, Root, Right) : 4 2 5 1 3 || = 3" $ do
            let t1 = Node (Node (Node Leaf 4 Leaf) 2 (Node Leaf 5 Leaf)) 1 (Node Leaf 3 Leaf)
            maxDepth t1 `shouldBe` 3
    
    describe "118. Pascal's Triangle" $ do
        it "Pascal's Triangle 1 = [[1]]" $ do
            pascalTriangle 1 `shouldBe` [[1]]
        it "Pascal's Triangle 2 = [[1],[1,1]]" $ do
            pascalTriangle 2 `shouldBe` [[1],[1,1]]
        it "Pascal's Triangle 3 = [[1],[1,1],[1,2,1]]" $ do
            pascalTriangle 3 `shouldBe` [[1],[1,1],[1,2,1]]
        it "Pascal's Triangle 4 = [[1],[1,1],[1,2,1],[1,3,3,1]]" $ do
            pascalTriangle 4 `shouldBe` [[1],[1,1],[1,2,1],[1,3,3,1]]
        it "Pascal's Triangle 5 = [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]" $ do
            pascalTriangle 5 `shouldBe` [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]

    describe "119. Pascal's Triangle II" $ do
        it "Pascal's Triangle 1 = [1]" $ do
            pascalTriangle2 1 `shouldBe` [1]
        it "Pascal's Triangle 2 = [1, 1]" $ do
            pascalTriangle2 2 `shouldBe` [1, 1]
        it "Pascal's Triangle 3 = [1, 2, 1]" $ do
            pascalTriangle2 3 `shouldBe` [1, 2, 1]
        it "Pascal's Triangle 4 = [1, 3, 3, 1]" $ do
            pascalTriangle2 4 `shouldBe` [1, 3, 3, 1]
        it "Pascal's Triangle 5 = [1, 4, 6, 4, 1]" $ do
            pascalTriangle2 5 `shouldBe` [1, 4, 6, 4, 1]

    describe "121. Best Time to Buy and Sell Stock" $ do
        it "the max profit of [7,1,5,3,6,4] is 5" $ do
            maxProfit [7,1,5,3,6,4] `shouldBe` Just 5
        it "the max profit of [7,6,4,3,1] is Nothing/0" $ do
            maxProfit [7,6,4,3,1] `shouldBe` Nothing
    
    describe "125. Valid Palindrome" $ do
        it "[A man, a plan, a canal: Panama] is a palindrome" $ do
            isPalindrome "A man, a plan, a canal: Panama" `shouldBe` True
        it "[race a car] is not a palindrome" $ do
            isPalindrome "race a car" `shouldBe` False 