module Main where

import Lib
import Criterion.Main

s10 = searchInsertPosition [1..10]
s50 = searchInsertPosition [1..50]

main :: IO ()
main =
  defaultMain
    [ bgroup
        "searchInsertPosition"
        [ bench "head10" $ whnf s10 0,
          bench "mid10" $ whnf s10 5,
          bench "tail10" $ whnf s10 11,
          bench "head50" $ whnf s50 0,
          bench "mid50" $ whnf s50 25,
          bench "tail50" $ whnf s50 51
        ]
    ]