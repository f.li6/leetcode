module Lib where

-- import Data.List

import Data.Char (toLower)
import Data.List
import Data.Typeable (typeOf5)
import Debug.Trace (trace)
import Data.Bits
import Data.Set (fromList, toList)

-- 20. Valid Parentheses
validParentheses :: String -> Bool
validParentheses = null . foldr go []
  where
    go '(' (')' : xs) = xs
    go '[' (']' : xs) = xs
    go '{' ('}' : xs) = xs
    go x xs = x : xs

-- 21. Merge Two Sorted Lists
merge2SortedLists :: [Integer] -> [Integer] -> [Integer]
merge2SortedLists xs [] = xs
merge2SortedLists [] ys = ys
merge2SortedLists (x : xs) (y : ys)
  | x <= y = x : merge2SortedLists xs (y : ys)
  | otherwise = y : merge2SortedLists (x : xs) ys

-- 26. Remove Duplicates from Sorted Array
removeDuplicates :: [Integer] -> [Integer]
removeDuplicates [] = []
removeDuplicates [x] = [x]
removeDuplicates (x : y : xs)
  | x == y = removeDuplicates (x : xs)
  | otherwise = x : removeDuplicates (y : xs)

-- 27. Remove Element
removeElement :: (Ord a) => a -> [a] -> [a]
removeElement _ [] = []
removeElement target (x : xs)
  | x == target = removeElement target xs
  | otherwise = x : removeElement target xs

-- 28. Implement strStr()
strStr :: String -> String -> Maybe Int
strStr xs ys = findIndex (isPrefixOf ys) (tails xs)

-- 35. Search Insert Position
-- O(n)
searchInsertPosition :: [Int] -> Int -> Int
searchInsertPosition [] y = 0
searchInsertPosition (x : xs) y
  | x < y = 1 + searchInsertPosition xs y
  | otherwise = 0

searchInsertPositionB :: [Int] -> Int -> Int -> Int -> Int
searchInsertPositionB xs y lo hi
  | lo > hi = lo
  | y <= xs !! mid = searchInsertPositionB xs y lo (mid - 1)
  | otherwise = searchInsertPositionB xs y (mid + 1) hi
  where
    mid = (lo + hi) `div` 2

-- 53. Maximum Subarray
-- O(n^3)
maxSubArray :: [Int] -> Int
maxSubArray = maximum . map sum . continuousSubSeqs

continuousSubSeqs :: [a] -> [[a]]
continuousSubSeqs = filter (not . null) . concatMap inits . tails

-- O(n)
maxSubArrayDK :: [Int] -> Int
maxSubArrayDK [] = undefined

half :: [a] -> ([a], [a])
half xs = splitAt (length xs `div` 2) xs

-- 58. Length of Last Word
lengthOfLastWord :: String -> Int
lengthOfLastWord = length . last . words

-- 66. Plus One
plusOne :: [Integer] -> [Integer]
plusOne xs = map (read . (: [])) plusone
  where
    plusone = show (read (concatMap show xs) + 1)

-- 94. Binary Tree Inorder Traversal
data BinaryTree a = Leaf | Node (BinaryTree a) a (BinaryTree a) deriving (Eq, Ord, Show)

insertBT :: Ord a => a -> BinaryTree a -> BinaryTree a
insertBT b Leaf = Node Leaf b Leaf
insertBT b (Node left a right)
  | b < a = Node (insertBT b left) a right
  | b > a = Node left a (insertBT b right)
  | otherwise = Node left a right -- b == a

inorderTraversal :: BinaryTree a -> [a]
inorderTraversal Leaf = []
-- [] ++ [a] ++ [] = [a]
inorderTraversal (Node left a right) =
  inorderTraversal left
    ++ [a]
    ++ inorderTraversal right

-- 104. Maximum Depth of Binary Tree
maxDepth :: BinaryTree a -> Int
maxDepth Leaf = 0
maxDepth (Node left x right) = 1 + max (maxDepth left) (maxDepth right)

-- 108. Convert Sorted Array to Binary Search Tree
sortedArrayToBST :: Ord a => [a] -> BinaryTree a
sortedArrayToBST = foldr insertBT Leaf

-- 110. Balanced Binary Tree

-- 111. Minimum Depth of Binary Tree

-- 112. Path Sum

-- 118. Pascal's Triangle
pascalTriangleReverse :: Int -> [[Int]]
pascalTriangleReverse 1 = [[1]]
pascalTriangleReverse x = pascalTriangle2 x : pascalTriangleReverse (x - 1)

next :: Num a => [a] -> [a]
next xs = zipWith (+) (0 : xs) (xs ++ [0]) -- column addition

pascalTriangle :: Int -> [[Integer]]
pascalTriangle x = take x (iterate next [1]) -- [f(x), f(f(x)), f(f(f(x)))...]; infinite list

-- 119. Pascal's Triangle II
pascalTriangle2 :: Int -> [Int]
pascalTriangle2 1 = [1]
pascalTriangle2 2 = [1, 1]
pascalTriangle2 x = 1 : (orderedCombination . pascalTriangle2) (x - 1) ++ [1]

orderedCombination :: [Int] -> [Int]
orderedCombination (x : y : xs) = (x + y) : orderedCombination (y : xs)
orderedCombination _ = [] -- equivalent to "otherwise"

-- 121. Best Time to Buy and Sell Stock
maxProfit :: [Int] -> Maybe Int
maxProfit xs =
  if all (< 0) res
    then Nothing
    else Just (maximum res)
  where
    res = possibleProfits xs

possibleProfits :: [Int] -> [Int]
possibleProfits (x : xs) = map (\y -> y - x) xs ++ possibleProfits xs
possibleProfits _ = []

maxProfit2 :: (Ord a, Num a) => [a] -> a
maxProfit2 xs = maximum $ zipWith (-) xs (scanl1 min xs)

-- (scanl1 min xs) to find the best day to buy in
-- zipWith (-) xs ys to find the best day to sell out

-- 125. Valid Palindrome
isPalindrome :: [Char] -> Bool
isPalindrome xs =
  all (== True) $
    zipWith (==) go (reverse go)
  where
    go = [x | x <- map toLower xs, x `elem` ['a' .. 'z']]

-- 136. Single Number
{-
  a ^ b ^ c ^ a ^ b     # Commutativity
= a ^ a ^ b ^ b ^ c     # Using x ^ x = 0
= 0 ^ 0 ^ c             # Using x ^ 0 = x (and commutativity)
= c
-}
singleNumber :: [Int] -> Int
singleNumber = foldr1 xor

-- 137. Single Number II

singleNumber2 :: [Float] -> Float
singleNumber2 xs = (((*3) . sum) unique_xs - sum xs) / 2
  where unique_xs = (toList . fromList) xs